# YAML learning
This repository is devoted to examples of YAML.

YAML is used for storing configuration of projects.
YAML is much more human readable than .JSON .

PREREQUISITES:

There are no prerequisites for learning YAML. 

WHO SHOULD LEARN YAML ?

Every person who is in field of DevOps should have knowledge of YAML.
In the next documents, you will be introduced to YAML, it's datatypes, Tools and Technologies employing YAML and Creating data in YAML.

WHAT IS YAML?
YAML stands for 'YAML AIN'T MARKUP LANGUAGE'. It is a simple text based human readable language
for exchanging data between computers and people.
It is not a 'programming language'. It is mainly used to store configuration information and is
easily readable to humans.
It is important to learn this format as any change to this format can cause your application to 
malfunction.



Additional descriptions will be added to this file sooner.
